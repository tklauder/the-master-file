https://bsnider.cs.georgefox.edu/courses/csis460-operating-systems/

<table class="table table-hover visible-xs">
<tbody>
<tr class=""><td>
<h5>Week 1 · Tue</h5>
<p>Introduction &amp; History</p>
<p>
<em>Reading</em>:&nbsp;Ch. 1.1–1.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 1 · Thu</h5>
<p>System Concepts</p>
<p>
<em>Reading</em>:&nbsp;Ch. 1.3–1.5<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 2 · Tue</h5>
<p>System Services</p>
<p>
<em>Reading</em>:&nbsp;Ch. 1.6<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 2 · Thu</h5>
<p>System Structure</p>
<p>
<em>Reading</em>:&nbsp;Ch. 1.7–1.12<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 3 · Tue</h5>
<p>Processes</p>
<p>
<em>Reading</em>:&nbsp;Ch. 2.1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 3 · Thu</h5>
<p>Threads</p>
<p>
<em>Reading</em>:&nbsp;Ch. 2.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 4 · Tue</h5>
<p>Interprocess Communication</p>
<p>
<em>Reading</em>:&nbsp;Ch. 2.3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 4 · Thu</h5>
<p>Scheduling</p>
<p>
<em>Reading</em>:&nbsp;Ch. 2.4–2.7<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 5 · Tue</h5>
<p>Memory Abstraction</p>
<p>
<em>Reading</em>:&nbsp;Ch. 3.1–3.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 5 · Thu</h5>
<p>Virtual Memory &amp; Paging</p>
<p>
<em>Reading</em>:&nbsp;Ch. 3.3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 6 · Tue</h5>
<p>Page Replacement Algorithms</p>
<p>
<em>Reading</em>:&nbsp;Ch. 3.4<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 6 · Thu</h5>
<p>Mid-semester break<em>—no classes</em></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 7 · Tue</h5>
<p>Paging System Issues; Segmentation</p>
<p>
<em>Reading</em>:&nbsp;Ch. 3.5–3.9<br>
</p>
</td></tr>
<tr class="warning"><td>
<h5>Week 7 · Thu</h5>
<p>Midterm exam</p>
<p>
<em>Reading</em>:&nbsp;Ch. 1–3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 8 · Tue</h5>
<p>Files &amp; Directories</p>
<p>
<em>Reading</em>:&nbsp;Ch. 4.1–4.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 8 · Thu</h5>
<p>File System Implementation</p>
<p>
<em>Reading</em>:&nbsp;Ch. 4.3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 9 · Tue</h5>
<p>File System Management</p>
<p>
<em>Reading</em>:&nbsp;Ch. 4.4<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 9 · Thu</h5>
<p>Example File Systems</p>
<p>
<em>Reading</em>:&nbsp;Ch. 4.5–4.7; <a href="https://en.wikipedia.org/wiki/Ext4">ext4</a>; <a href="https://en.wikipedia.org/wiki/Btrfs">Btrfs</a><br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 10 · Tue</h5>
<p>I/O Hardware</p>
<p>
<em>Reading</em>:&nbsp;Ch. 5.1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 10 · Thu</h5>
<p>I/O Software</p>
<p>
<em>Reading</em>:&nbsp;Ch. 5.2–5.3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 11 · Tue</h5>
<p>Disks &amp; Clocks</p>
<p>
<em>Reading</em>:&nbsp;Ch. 5.4–5.5<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 11 · Thu</h5>
<p>User I/O Devices &amp; Power Management</p>
<p>
<em>Reading</em>:&nbsp;Ch. 5.6–5.10<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 12 · *</h5>
<p>Spring break—<em>no classes</em></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 13 · Tue</h5>
<p>Deadlock Detection &amp; Recovery</p>
<p>
<em>Reading</em>:&nbsp;Ch. 6.1–6.4<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 13 · Thu</h5>
<p>Deadlock Avoidance &amp; Prevention</p>
<p>
<em>Reading</em>:&nbsp;Ch. 6.5–6.9<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 14 · Tue</h5>
<p>Virtualization</p>
<p>
<em>Reading</em>:&nbsp;Ch. 7.1–7.10<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 14 · Thu</h5>
<p>Multiprocessor Systems</p>
<p>
<em>Reading</em>:&nbsp;Ch. 8.1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 15 · Tue</h5>
<p>Security</p>
<p>
<em>Reading</em>:&nbsp;Ch. 9.1–9.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 15 · Thu</h5>
<p>Case Studies</p>
<p>
<em>Reading</em>:&nbsp;Ch. 10.1–10.2; 11.1; 11.3<br>
</p>
</td></tr>
<tr class="warning"><td>
<h5>Week 16 · TBD</h5>
<p>Final exam</p>
<p>
<em>Reading</em>:&nbsp;*<br>
</p>
</td></tr>
</tbody>
</table>