https://bsnider.cs.georgefox.edu/courses/csis440-artificial-intelligence/

<table class="table table-hover visible-xs">
            <tbody>
<tr class=""><td>
<h5>Week 1 · Tue</h5>
<p>Introduction and History</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 1 · Thu</h5>
<p>Agents and Environments</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 2.1–2.3; Ch. 27.1<br>
<em>Mitchell</em>:&nbsp;Prologue<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 2 · Tue</h5>
<p>Agent Structure; Philosophy of AI</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 2.4; Ch. 27.2<br>
<em>Mitchell</em>:&nbsp;Ch. 1–3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 2 · Thu</h5>
<p>Problem Solving</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 3.1–3.3<br>
<em>Mitchell</em>:&nbsp;Ch. 4<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 3 · Tue</h5>
<p>Blind Search</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 3.4; Ch. 27.3<br>
<em>Mitchell</em>:&nbsp;Ch. 5–6<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 3 · Thu</h5>
<p>Heuristic Search</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 3.5–3.6<br>
<em>Mitchell</em>:&nbsp;Ch. 7<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 4 · Tue</h5>
<p>Local Search and Optimization</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 4.1<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 4 · Thu</h5>
<p>Online Search</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 4.5<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 5 · Tue</h5>
<p>Adversarial Search</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 5.1–5.4<br>
<em>Mitchell</em>:&nbsp;Ch. 8<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 5 · Thu</h5>
<p>Stochasticity and Partial Observability</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 5.5–5.7<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 6 · Tue</h5>
<p>Constraint Satisfaction</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 6.1–6.2<br>
<em>Mitchell</em>:&nbsp;Ch. 9<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 6 · Thu</h5>
<p>Mid-semester break<em>—no classes</em></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 7 · Tue</h5>
<p>Knowledge-Based Agents</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 7.1–7.2<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 7 · Thu</h5>
<p>Propositional Logic</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 7.3–7.4<br>
<em>Mitchell</em>:&nbsp;Ch. 10<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 8 · Tue</h5>
<p>Propositional Logic-Based Agents</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 7.7<br>
</p>
</td></tr>
<tr class="warning"><td>
<h5>Week 8 · Thu</h5>
<p>Midterm exam</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 1–7<br>
<em>Mitchell</em>:&nbsp;Ch. 1–7<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 9 · Tue</h5>
<p>First-Order Logic</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 8<br>
<em>Mitchell</em>:&nbsp;Ch. 11<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 9 · Thu</h5>
<p>First-Order Inference</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 9<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 10 · Tue</h5>
<p>Knowledge Represenation</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 10.1<br>
<em>Mitchell</em>:&nbsp;Ch. 12<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 10 · Thu</h5>
<p>Planning</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 11.1–11.3<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 11 · Tue</h5>
<p>Machine Learning</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 19.1–19.5<br>
<em>Mitchell</em>:&nbsp;Ch. 13<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 11 · Thu</h5>
<p>Machine Learning Models and Systems</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 19.6–19.9<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 12 · *</h5>
<p>Spring break<em>—no classes</em></p>
<p>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 13 · Tue</h5>
<p>Deep Learning and Deep Neural Networks</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 21.1–21.2<br>
<em>Mitchell</em>:&nbsp;Ch. 14<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 13 · Thu</h5>
<p>DNN Architectures, Algorithms, and Techniques</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 21.3–21.8<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 14 · Tue</h5>
<p>Selected Topics: Natural Language Processing</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 23<br>
<em>Mitchell</em>:&nbsp;Ch. 15<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 14 · Thu</h5>
<p>Selected Topics: Deep Learning for NLP</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 24<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 15 · Tue</h5>
<p>Selected Topics: Robotics</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 26<br>
<em>Mitchell</em>:&nbsp;Ch. 16<br>
</p>
</td></tr>
<tr class=""><td>
<h5>Week 15 · Thu</h5>
<p>Future of Artificial Intelligence</p>
<p>
<em>AIMA</em>:&nbsp;Ch. 27<br>
</p>
</td></tr>
<tr class="warning"><td>
<h5>Week 16 · TBD</h5>
<p>Final exam</p>
<p>
<em>AIMA</em>:&nbsp;*<br>
<em>Mitchell</em>:&nbsp;*<br>
</p>
</td></tr>
</tbody>
            </table>